import ela
import json
from collections import namedtuple

def _json_object_hook(d): return namedtuple('X', d.keys())(*d.values())
def json2obj(data): return json.loads(data, object_hook=_json_object_hook)

name = input()
age = input()

doc = {
    "name": name,
    "age": age
}

# ela.document_add("testpython","type", doc, None)

res = ela.document_search("testpython","type", name)



# res = json2obj(res)

# print(res.name)