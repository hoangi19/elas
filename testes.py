from elasticsearch import Elasticsearch

import json

es = Elasticsearch()

def esCreateIndex(index):
    es.indices.create(index)

def esGet(myindex, mytype, myid):
    return es.get(index=myindex, doc_type=mytype, id=myid)

def esSearch(term):
    _body = json.dumps({
        "query": {
            "match":{
                "comment": term
            }
        }
    })
    return es.search(index="_all", doc_type="", body = _body)

def esUpdate(_body, _index, _type, _id):
    es.update(index=_index, doc_type=_type, id=_id, body={"doc": _body})


if __name__ == '__main__':
    # esCreateIndex('testpython')
    # es.get("testpython","type",1)
    
    # res = esSearch("hey")
    # print(res)

    body = json.dumps({
        "comment": "fuku"
    })
    esUpdate(body, 'testpython', 'type', 1)
    # esGet('testpython', 'type', 1)

    exit
