from elasticsearch import Elasticsearch

# create an instance of elasticsearch and assign it to port 9200
ES_HOST = {"host": "localhost", "port": 9200}
es = Elasticsearch(hosts=[ES_HOST])


def create_index(index_name):
    """Functionality to create index."""
    resp = es.indices.create(index=index_name)
    print(resp)


def document_add(index_name, doc_type, doc, doc_id=None):
    """Funtion to add a document by providing index_name,
    document type, document contents as doc and document id."""
    resp = es.index(index=index_name, doc_type=doc_type, body=doc, id=doc_id)
    print(resp)


def document_view(index_name, doc_type, doc_id):
    """Function to view document."""
    resp = es.get(index=index_name, doc_type=doc_type, id=doc_id)
    document = resp["_source"]
    print(document)


def document_update(index_name, doc_type, doc_id, doc=None, new=None):
    """Function to edit a document either updating existing fields or adding a new field."""
    if doc:
        resp = es.index(index=index_name, doc_type=doc_type,
                        id=doc_id, body=doc)
        print(resp)
    else:
        resp = es.update(index=index_name, doc_type=doc_type,
                         id=doc_id, body={"doc": new})


def document_delete(index_name, doc_type, doc_id):
    """Function to delete a specific document."""
    resp = es.delete(index=index_name, doc_type=doc_type, id=doc_id)
    print(resp)


def delete_index(index_name):
    """Delete an index by specifying the index name"""
    resp = es.indices.delete(index=index_name)
    print(resp)

def document_search(index_name, type, text):
    """Function to search."""
    doc = {
        "query": {
            "query_string": {
                "query": text
            }
        }
    }
    resp = es.search(index=index_name, doc_type=type, body=doc)
    # print(resp)
    return resp